<h2> Utils </h2>
mdadm -Esv

<h2> Install </h2>

sudo mdadm --stop /dev/md* <br/>
sudo mdadm --zero-superblock /dev/sd[abcd]2

echo -e "o\nn\n\n\n\n+1G\n\nn\n\n\n\n\nw" | fdisk /dev/sda <br/>
echo -e "o\nn\n\n\n\n+1G\n\nn\n\n\n\n\nw" | fdisk /dev/sdb <br/>
echo -e "o\nn\n\n\n\n+1G\n\nn\n\n\n\n\nw" | fdisk /dev/sdc <br/>
echo -e "o\nn\n\n\n\n+1G\n\nn\n\n\n\n\nw" | fdisk /dev/sdd

apt install zstd dosfstools <br/>
mkfs.vfat /dev/sda1 <br/>
mkfs.vfat /dev/sdb1 <br/>
mkfs.vfat /dev/sdc1 <br/>
mkfs.vfat /dev/sdd1

mkdir system

| mdadm                                                                                                                  | btrfs                                                            |
|------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------|
| mdadm --create /dev/md0 --level=0 --raid-devices=4 /dev/sd[abcd]2 <br/> mkfs.ext4 /dev/md0 <br/> mount /dev/md0 system | mkfs.btrfs -m raid10 /dev/sd[abcd]2 <br/> mount /dev/sda2 system |

mkdir system/boot <br/>
mount /dev/sda1 system/boot <br/>
wget https://raw.githubusercontent.com/tokland/arch-bootstrap/master/arch-bootstrap.sh <br/>
chmod +x arch-bootstrap.sh <br/>
./arch-bootstrap.sh system <br/>
mount -t proc proc system/proc <br/>
mount -t sysfs sys system/sys <br/>
mount -o bind /dev system/dev <br/>
chroot system

pacman-key --init <br/>
pacman-key --populate archlinux <br/>
sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 5/' /etc/pacman.conf<br/>
pacman -S base base-devel git grub intel-ucode openssh sudo vim wget curl linux linux-firmware mdadm arch-install-scripts rsync htop haproxy docker neofetch python bash-completion screen tmux linux-headers efibootmgr

| mdadm                                                        |
|--------------------------------------------------------------|
| pacman -S lvm2<br/>mdadm --examine --scan >> /etc/mdadm.conf |

genfstab / >> /etc/fstab

| mdadm                                                                                                   |
|---------------------------------------------------------------------------------------------------------|
| #/etc/mkinitcpio.conf <br/> MODULES+="dm_mod crc32_generic crc32c-intel" <br/> HOOKS+="mdadm_udev lvm2" |

mkinitcpio -p linux <br/>
mkdir /boot/efi <br/>
mkdir /boot/efi/EFI <br/>
> **_NOTE:_** USE bootloader-id FROM ALREADY INSTALLED OS, IN THIS CASE DEBIAN <br/>
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=debian <br/>

grub-mkconfig -o /boot/grub/grub.cfg

cat > /etc/systemd/network/eth0.network << EOF <br/>
[Match] <br/>
Name=eth0 en*

[Network] <br/>
DHCP=true <br/>
EOF

systemctl enable systemd-networkd <br/>
systemctl enable sshd <br/>
echo 'esaul.waw.pl' > /etc/hostname <br/>
ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime <br/>
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen <br/>
locale-gen <br/>
echo "LANG=en_US.UTF-8" > /etc/locale.conf

cat > /etc/ssh/sshd_config << EOF <br/>
Port 22 <br/>
ListenAddress 0.0.0.0 <br/>
PubkeyAuthentication yes <br/>
AuthorizedKeysFile .ssh/authorized_keys <br/>
PasswordAuthentication no <br/>
KbdInteractiveAuthentication no <br/>
UsePAM no <br/>
X11Forwarding yes <br/>
PrintMOTD no <br/>
TCPKeepAlive yes <br/>
Subsystem    sftp  /usr/lib/ssh/sftp-server <br/>
EOF
